from ruamel import yaml
#import yaml

import pprint

# At this point either ruamel.yaml or pyyaml is in scope.

input_yaml = """
some: value
another: thing
sub:
    dict: value
empty_list: []
list_of:
- a_string
- a: dict
- []
- [ "foo" ]
"""

class Representer:
    def __init__(self, file_index):
        self._file_index = file_index
        self.state = "init"
        self.output = []
        self.keys = []

    def handle_event(self, event):
        handler = "_handle_{}_{}".format(self.state, event.__class__.__name__)
        handler = getattr(self, handler, None)
        if handler is None:
            raise Exception("No handler for {} in state {}".format(event, self.state))
        #print("Handling {} at {}:{} for state {}".format(event, event.start_mark.line, event.start_mark.column + 1, self.state))
        self.state = handler(event)

    def get_output(self):
        return self.output[0]

    def _handle_init_StreamStartEvent(self, ev):
        return "stream"

    def _handle_stream_DocumentStartEvent(self, ev):
        return "doc"

    def _handle_doc_MappingStartEvent(self, ev):
        newmap = {"_bst_pk": (self._file_index, ev.start_mark.line, ev.start_mark.column + 1)}
        self.output.append(newmap)
        return "wait_key"

    def _handle_wait_key_ScalarEvent(self, ev):
        self.keys.append(ev.value)
        return "wait_value"

    def _handle_wait_value_ScalarEvent(self, ev):
        key = self.keys.pop()
        self.output[-1][key] = (ev.value, self._file_index, ev.start_mark.line, ev.start_mark.column + 1)
        return "wait_key"

    def _handle_wait_value_MappingStartEvent(self, ev):
        new_state = self._handle_doc_MappingStartEvent(ev)
        key = self.keys.pop()
        self.output[-2][key] = self.output[-1]
        return new_state

    def _handle_wait_key_MappingEndEvent(self, ev):
        # We've finished a mapping, so pop it off the output stack
        # unless it's the last one in which case we leave it
        if len(self.output) > 1:
            self.output.pop()
            if type(self.output[-1]) == list:
                return "wait_list_item"
            else:
                return "wait_key"
        else:
            return "doc"

    def _handle_wait_value_SequenceStartEvent(self, ev):
        self.output.append([])
        self.output[-2][self.keys[-1]] = ([], self._file_index, ev.start_mark.line, ev.start_mark.column + 1)
        return "wait_list_item"

    def _handle_wait_list_item_SequenceStartEvent(self, ev):
        self.keys.append(len(self.output[-1]))
        self.output.append([])
        self.output[-2].append(([], self._file_index, ev.start_mark.line, ev.start_mark.column + 1))
        return "wait_list_item"

    def _handle_wait_list_item_SequenceEndEvent(self, ev):
        # When ending a sequence, we need to pop a key because we retain the
        # key until the end so that if we need to mutate the underlying entry
        # we can.
        key = self.keys.pop()
        self.output.pop()
        if type(key) is int:
            return "wait_list_item"
        else:
            return "wait_key"

    def _transform_list(self):
        if len(self.output[-1]):
            return
        self.output[-1] = []
        self.output[-2][self.keys[-1]] = self.output[-1]
        
    def _handle_wait_list_item_ScalarEvent(self, ev):
        self._transform_list()
        self.output[-1].append((ev.value, self._file_index, ev.start_mark.line, ev.start_mark.column + 1))
        return "wait_list_item"

    def _handle_wait_list_item_MappingStartEvent(self, ev):
        self._transform_list()
        new_state = self._handle_doc_MappingStartEvent(ev)
        self.output[-2].append(self.output[-1])
        return new_state

    def _handle_doc_DocumentEndEvent(self, ev):
        if len(self.output) != 1:
            raise Exception("Document not loaded properly")
        return "stream"

    def _handle_stream_StreamEndEvent(self, ev):
        return "init"

def load(src, Loader=None):
    representer = Representer(0)
    for event in yaml.parse(src, Loader=yaml.CBaseLoader):
        representer.handle_event(event)
    return representer.get_output()
    
if __name__ == "__main__":
    
    representer = Representer(0)
    
    for event in yaml.parse(input_yaml, Loader=yaml.CBaseLoader):
        representer.handle_event(event)

    print("YAML:\n{}\n".format(input_yaml))
    print("Result: {}".format(pprint.pformat(representer.get_output())))

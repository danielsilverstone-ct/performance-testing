from ruamel.yaml import load
from ruamel.yaml import CBaseLoader as Loader

#from yaml import load
#from yaml import CBaseLoader as Loader

#from dodgy_loader import load
#Loader = None

from collections import deque
from functools import cmp_to_key
import sys

toplevel = [e for e in sys.argv[1:]]
depkinds = ["depends", "runtime-depends", "build-depends"]

rundeps=["depends", "runtime-depends"]
builddeps=["depends", "build-depends"]


to_load=deque([sys.intern(t) for t in toplevel])
loaded = {}
nodes = {}
ncounter = 0
ndeps = 0

nsorts = 0

def sort_counter(a,b):
    global nsorts
    nsorts += 1
    if a < b:
        return -1
    elif a > b:
        return 1
    return 0

ndep_spread = {}

while len(to_load) > 0:
    fname = to_load.popleft()
    if fname not in loaded:
        nodes[fname] = "n{}".format(ncounter)
        ncounter += 1
        with open(fname, "r") as fh:
            content = fh.read()
        data = load(content, Loader=Loader)
        loaded[fname] = data
        lenl = 0
        for depkind in depkinds:
            l = data.get(depkind, [])
            if type(l) is tuple:
                l = l[0]
            l.sort(key=cmp_to_key(sort_counter))
            lenl_ = len(l)
            ndeps += lenl_
            lenl += lenl_
            for i, dep in enumerate(l):
                if type(dep) is tuple:
                    dep = dep[0]
                dep = sys.intern(dep)
                l[i] = dep
                to_load.append(dep)
        ndep_spread[lenl] = ndep_spread.get(lenl, 0) + 1

#print("graph tree {")
#for fname, data in loaded.items():
#    print("  {} [label=\"{}\"];".format(nodes[fname], fname))
#    for depkind in depkinds:
#        for dep in data.get(depkind, []):
#            if depkind in rundeps:
#                print("  {} -- {};".format(nodes[fname], nodes[dep]))
#            if depkind in builddeps:
#                print("  {} -- {} [style=dotted];".format(nodes[fname], nodes[dep]))
#print("}")

print("Loaded {} elements, {} dependency relationships, averaging {} deps per element".format(ncounter,ndeps, int((ndeps/ncounter)*10)/10))
#print("Unscientific sorter comparison count is: {}".format(nsorts))
#for k in sorted(ndep_spread.keys()):
#    print("{} elements have {} deps".format(ndep_spread[k], k))
